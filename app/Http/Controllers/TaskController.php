<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Task;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$task= Task::all();
    //return view('tasks.index',['tasks' => $task]);
       $id =Auth::id();//only for log in
       if (Gate::denies('manager')) {
           $tasks=Task::all();
           return view('tasks.index',['tasks' => $tasks]) ;
    }
        $tasks= User::find($id)->tasks;
        return view('tasks.index',['tasks' => $tasks]) ;  
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task= new Task();
        $id =Auth::id();
        $task->title = $request->title;
        $task->status = 0;
        $task->user_id = $id;
        $task->save();
        return redirect('tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        return view('tasks.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('manager')) {
            if ($request->ajax()){
            abort(403,"you can't do that");}}
        $task = Task::find($id);
        if(!$task->user->id == Auth::id())return (redirect('tasks'));
        $task->update($request->except(['_token']));
        return redirect('tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");}
        $task->delete();
        return redirect('tasks');
    }
  public function mytasks(){  
    $id =Auth::id();
    $tasks= User::find($id)->tasks;
    return view('tasks.index',['tasks' => $tasks]) ; 
    }
  
}
