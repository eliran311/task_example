
@extends('layouts.app')
@section('content')


<h1>
This is your task list
</h1>

@if (Request::is('tasks'))
<a href="{{action('TaskController@mytasks')}}">My Tasks</a>
@else
<a href="{{action('TaskController@index')}}">All Tasks</a>

 @endif
<ul>@foreach($tasks as $task)  
<li> {{$task->title}} 

<a  class="edittask" href = "{{route('tasks.edit',$task->id)}}" >   edit </a> 
@if ($task->status==1)
           <label> Done!</label>
       @else
           <button style="text-decoration: underline" id ="{{$task->id}}" value="0"> Mark as done</button>
       @endif
        
</li>
@can('manager')<form method = 'post' action = "{{action('TaskController@destroy',$task->id)}}" >

@csrf   
@method('DELETE')    
<div class = "form-group">    
    <input type = "submit" class = "form-control" name = "submit" value = "Delete">
</div>

</form>   @endcan 


@endforeach <br>
<a class="create" href = "{{route('tasks.create')}}">   create a new book </a>
         

<style>
a.edittask{
  color: #2da1c1;
  font-size: medium;
  text-decoration:underline ;
  padding:0 20px;
  right:1200px;    
}
.form-group{
    width: 70px;
  height: 50px;
  
}

a.create{
     border-bottom-color: coral;
     font-weight: bold;
}
.links {
         padding: 20 100px ;
         font-size: medium;
        letter-spacing: .1rem;
               
            }
</style>

  


</ul>
<script>
       $(document).ready(function(){
           $("button").click(function(event){
               $.ajax({
                   url:  "{{url('tasks')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   data: JSON.stringify({'status':(event.target.value-1)*-1, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
               location.reload();

           });

       });
   </script>
 

@endsection

